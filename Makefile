.PHONY: init build version test generate-mocks

init:
	git flow init -df
	git config gitflowlab.version.filename "pkg/gfl/version.go"
	git config gitflowlab.version.regex '[Vv]ersion.*\"(.*)\"'

version:
	@cat pkg/version.go | grep Version | cut -d '"' -f2

build:
	go build -o gfl ./cmd/gfl

test:
	go test ./pkg/...

generate-mocks:
	mockery --inpackage --all --testonly