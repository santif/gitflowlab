# GitFlowLab

GitFlow with GitLab

This tool integrates [Git flow AVH Edition](https://github.com/petervanderdoes/gitflow-avh)
command line interface with [GitLab](https://about.gitlab.com/) platform, reducing manual
tasks involved in the process and performing validations by using Merge Requests.


## Prerequisites

- [Git flow AVH edition](https://github.com/petervanderdoes/gitflow-avh)

## Install

### Using `go install`

```
go install gitlab.com/santif/gitflowlab/cmd/gfl@latest
```

## Check installation

Run

```
gfl version
```

## Configuration

Add a `~/.gitflowlab.yaml` with one entry per server:

```yaml
servers:
  gitlab.com:
    token: "...personal token..."
  gitlab.example:
    token: "...personal token..."
    https: false
```

To create a personal token, go to `https://GITLAB-SERVER/-/profile/personal_access_tokens`,
enter a Name, select `api` scope and press "Create personal access token" button.

## Repository setup

From the working directory in local repositoy, run `gfl setup`. Flags allowed:

```
-n, --version-filename string   relative path to a file containing the artifact's version (example: package.json)
-r, --version-regex string      version regex with one group (default "[Vv]ersion.*\\\"(.*)\\\"")
```

## Usage

**`gfl`** is a replacement of `git flow` tool.

The usage is similar to `git flow`. This tool adds:

- Merge request creation after `publish`.
- Merge request validations before `finish` (approval status, open discussions,
  conflicts, WIP status, etc).
- Auto increment version in `gfl release|hotfix start` commands.
- Automatic push of branches and tags.

### Examples

To setup a project to use GitFlow and GitFlowLab with default settings:

```
gfl setup
```

Setup a project specifying the file that contains artifact's version.
This will allow `gfl` to automatically increase the version in releases and hotfixes:

```
gfl setup -n pkg/version.go
```

```
gfl setup -n package.json
```

etc.

Create a feature branch:

```
gfl feature start my-feature
```

Publish the feature and create a Merge Request targeting `develop` branch:

```
gfl feature publish
```

Finish a feature and merge it to `develop` branch (from feature branch).
The Merge Request has to be approved by the assignees, without merge conflicts,
and cannot be in WIP or Draft status:

```
gfl feature finish
```

Create a release auto-incrementing the MINOR version:

```
gfl release start
```

or

```
gfl release start minor
```

Create a release auto-incrementing the MAJOR version:

```
gfl release start major
```

Publish the release and create a Merge Request targeting `main` branch:

```
gfl release publish
```

Finish a release and merge it to `main` branch, pushing branches and tags:

```
gfl release finish
```

To start a hotfix branch, auto-incrementing the PATCH version:

```
gfl hotfix start
```

To publish the hotfix and create a Merge Request targeting `main` branch:

```
gfl hotfix publish
```

To finish a hotfix and merge it to `main` branch, pushing branches and tags:

```
gfl hotfix finish
```

`gfl` allows to send arguments to `git flow` command after `--`, for example:

```
gfl feature start my-feature -- -v
```
