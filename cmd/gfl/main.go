package main

import (
	"log"

	"github.com/spf13/cobra"
)

func main() {
	log.SetFlags(0)
	cobra.CheckErr(rootCmd.Execute())
}
