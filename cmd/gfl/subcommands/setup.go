package subcommands

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/santif/gitflowlab/pkg/gfl"
)

var setupCmdVersionRegex string
var setupCmdVersionFilename string
var setupMergeRequestApprovalRequired bool

func SetupCommand(gitflowCommand gfl.GitFlowCommand) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "setup",
		Short: "git flow setup",
		Long:  "git flow setup",
		Run: func(cmd *cobra.Command, args []string) {
			opts := gfl.SetupOptions{
				VersionRegexString: setupCmdVersionRegex,
				VersionFilename:    setupCmdVersionFilename,
			}

			err := gitflowCommand.Setup(args, opts)
			if err != nil {
				log.Fatal(err)
			}
		},
	}

	cmd.Flags().StringVarP(&setupCmdVersionFilename, "version-filename", "n", "", "relative path to a file containing the artifact's version (example: package.json)")
	cmd.Flags().StringVarP(&setupCmdVersionRegex, "version-regex", "r", `[Vv]ersion.*\"(.*)\"`, "version regex with one group")
	cmd.Flags().BoolVarP(&setupMergeRequestApprovalRequired, "merge-request-approval-required", "a", true, "merge request approval required")

	return cmd
}
