package subcommands

import (
	"fmt"
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/santif/gitflowlab/pkg/gfl"
)

func PullCommand(cmdName string, gitflowCommand gfl.GitFlowCommand) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "pull",
		Short: fmt.Sprintf("%s pull", cmdName),
		Long:  fmt.Sprintf("%s pull", cmdName),
		Run: func(cmd *cobra.Command, args []string) {
			err := gitflowCommand.SetCommand(cmdName).Pull(args)
			if err != nil {
				log.Fatal(err)
			}

		},
	}

	return cmd
}
