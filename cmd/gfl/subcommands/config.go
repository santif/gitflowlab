package subcommands

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/santif/gitflowlab/pkg/gfl"
)

func UnsetConfigCommand(name string, subcommand string, option string, cfg gfl.Config, repo gfl.Repo) *cobra.Command {
	cmd := &cobra.Command{
		Use: name,
		Run: func(cmd *cobra.Command, args []string) {
			err := repo.DeleteConfig(subcommand, option)
			if err != nil {
				log.Fatal(err)
			}
		},
	}

	return cmd
}

func SetConfigStringCommand(name string, subcommand string, option string, defaultValue string, cfg gfl.Config, repo gfl.Repo) *cobra.Command {
	cmd := &cobra.Command{
		Use: name,
		Run: func(cmd *cobra.Command, args []string) {
			value := defaultValue
			if len(args) == 1 {
				value = args[0]
			}

			err := repo.SetConfig(subcommand, option, value)
			if err != nil {
				log.Fatal(err)
			}
		},
	}

	return cmd
}

func SetConfigFlagCommand(name string, subcommand string, option string, defaultValue bool, cfg gfl.Config, repo gfl.Repo) *cobra.Command {
	cmd := &cobra.Command{
		Use: name,
	}

	cmd.AddCommand(&cobra.Command{
		Use: "enable",
		Run: func(cmd *cobra.Command, args []string) {
			err := repo.SetConfig(subcommand, option, "true")
			if err != nil {
				log.Fatal(err)
			}
		},
	})

	cmd.AddCommand(&cobra.Command{
		Use: "disable",
		Run: func(cmd *cobra.Command, args []string) {
			err := repo.SetConfig(subcommand, option, "false")
			if err != nil {
				log.Fatal(err)
			}
		},
	})

	return cmd
}

func ConfigSetCommand(cfg gfl.Config, repo gfl.Repo) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "set",
		Short: "set config",
		Long:  "set config",
	}

	cmd.AddCommand(
		SetConfigFlagCommand(
			"mr-approval-required",
			"mergerequest",
			"approvalrequired",
			true,
			cfg,
			repo,
		))

	cmd.AddCommand(
		SetConfigStringCommand(
			"version-regex",
			"version",
			"regex",
			"[Vv]ersion.*\"(.*)\"",
			cfg,
			repo,
		))

	cmd.AddCommand(
		SetConfigStringCommand(
			"version-filename",
			"version",
			"filename",
			"",
			cfg,
			repo,
		))

	return cmd
}

func ConfigUnsetCommand(cfg gfl.Config, repo gfl.Repo) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "unset",
		Short: "unset config",
		Long:  "unset config",
	}

	cmd.AddCommand(
		UnsetConfigCommand(
			"mr-approval-required",
			"mergerequest",
			"approvalrequired",
			cfg,
			repo,
		))

	cmd.AddCommand(
		UnsetConfigCommand(
			"version-regex",
			"version",
			"regex",
			cfg,
			repo,
		))

	cmd.AddCommand(
		UnsetConfigCommand(
			"version-filename",
			"version",
			"filename",
			cfg,
			repo,
		))

	return cmd
}
