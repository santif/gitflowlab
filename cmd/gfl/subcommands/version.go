package subcommands

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/santif/gitflowlab/pkg/gfl"
)

func VersionCommand(gitflowCommand gfl.GitFlowCommand) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "version",
		Short: "git flow version",
		Long:  "git flow version",
		Run: func(cmd *cobra.Command, args []string) {
			err := gitflowCommand.Version()
			if err != nil {
				log.Fatal(err)
			}
		},
	}

	return cmd
}
