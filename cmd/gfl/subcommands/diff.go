package subcommands

import (
	"fmt"
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/santif/gitflowlab/pkg/gfl"
)

func DiffCommand(cmdName string, gitflowCommand gfl.GitFlowCommand) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "diff",
		Short: fmt.Sprintf("%s diff", cmdName),
		Long:  fmt.Sprintf("%s diff", cmdName),
		Run: func(cmd *cobra.Command, args []string) {
			err := gitflowCommand.SetCommand(cmdName).Diff(args)
			if err != nil {
				log.Fatal(err)
			}

		},
	}

	return cmd
}
