package subcommands

import (
	"fmt"
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/santif/gitflowlab/pkg/gfl"
)

func TrackCommand(cmdName string, gitflowCommand gfl.GitFlowCommand) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "track",
		Short: fmt.Sprintf("%s track", cmdName),
		Long:  fmt.Sprintf("%s track", cmdName),
		Run: func(cmd *cobra.Command, args []string) {
			err := gitflowCommand.SetCommand(cmdName).Track(args)
			if err != nil {
				log.Fatal(err)
			}

		},
	}

	return cmd
}
