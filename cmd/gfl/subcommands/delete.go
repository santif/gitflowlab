package subcommands

import (
	"fmt"
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/santif/gitflowlab/pkg/gfl"
)

func DeleteCommand(cmdName string, gitflowCommand gfl.GitFlowCommand) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "delete",
		Short: fmt.Sprintf("%s delete", cmdName),
		Long:  fmt.Sprintf("%s delete", cmdName),
		Run: func(cmd *cobra.Command, args []string) {
			err := gitflowCommand.SetCommand(cmdName).Delete(args)
			if err != nil {
				log.Fatal(err)
			}

		},
	}

	return cmd
}
