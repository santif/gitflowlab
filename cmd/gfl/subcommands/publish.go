package subcommands

import (
	"fmt"
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/santif/gitflowlab/pkg/gfl"
)

func PublishCommand(cmdName string, gitFlowCommand gfl.GitFlowCommand) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "publish",
		Short: fmt.Sprintf("%s publish", cmdName),
		Long:  fmt.Sprintf("%s publish", cmdName),
		Run: func(cmd *cobra.Command, args []string) {
			err := gitFlowCommand.SetCommand(cmdName).Publish(args)
			if err != nil {
				log.Fatal(err)
			}

		},
	}

	return cmd
}
