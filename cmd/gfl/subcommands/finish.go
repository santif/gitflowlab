package subcommands

import (
	"fmt"
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/santif/gitflowlab/pkg/gfl"
)

func FinishCommand(cmdName string, gitflowCommand gfl.GitFlowCommand) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "finish",
		Short: fmt.Sprintf("%s finish", cmdName),
		Long:  fmt.Sprintf("%s finish", cmdName),
		Run: func(cmd *cobra.Command, args []string) {
			err := gitflowCommand.SetCommand(cmdName).Finish(args)
			if err != nil {
				log.Fatal(err)
			}

		},
	}

	return cmd
}
