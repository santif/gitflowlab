package subcommands

import (
	"fmt"
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/santif/gitflowlab/pkg/gfl"
)

func RebaseCommand(cmdName string, gitflowCommand gfl.GitFlowCommand) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "rebase",
		Short: fmt.Sprintf("%s rebase", cmdName),
		Long:  fmt.Sprintf("%s rebase", cmdName),
		Run: func(cmd *cobra.Command, args []string) {
			err := gitflowCommand.SetCommand(cmdName).Rebase(args)
			if err != nil {
				log.Fatal(err)
			}

		},
	}

	return cmd
}
