package subcommands

import (
	"fmt"
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/santif/gitflowlab/pkg/gfl"
)

func CheckoutCommand(cmdName string, gitflowCommand gfl.GitFlowCommand) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "checkout",
		Short: fmt.Sprintf("%s checkout", cmdName),
		Long:  fmt.Sprintf("%s checkout", cmdName),
		Run: func(cmd *cobra.Command, args []string) {
			err := gitflowCommand.SetCommand(cmdName).Checkout(args)
			if err != nil {
				log.Fatal(err)
			}

		},
	}

	return cmd
}
