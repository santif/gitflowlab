package subcommands

import (
	"fmt"
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/santif/gitflowlab/pkg/gfl"
)

func RenameCommand(cmdName string, gitflowCommand gfl.GitFlowCommand) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "rename",
		Short: fmt.Sprintf("%s rename", cmdName),
		Long:  fmt.Sprintf("%s rename", cmdName),
		Run: func(cmd *cobra.Command, args []string) {
			err := gitflowCommand.SetCommand(cmdName).Rename(args)
			if err != nil {
				log.Fatal(err)
			}

		},
	}

	return cmd
}
