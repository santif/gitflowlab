package main

import (
	"fmt"
	"log"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/santif/gitflowlab/cmd/gfl/subcommands"
	"gitlab.com/santif/gitflowlab/pkg/gfl"
)

var cfgFile string
var verbose bool
var cfg gfl.Config
var repo gfl.Repo
var gitlabClient gfl.GitlabClient
var gitFlow gfl.GitFlow
var gitFlowCommand gfl.GitFlowCommand

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "gfl",
	Short: "Git flow with GitLab",
	Long:  "Git flow with GitLab",
}

// feature
var featureCmd = &cobra.Command{
	Use:   "feature",
	Short: "Git flow feature subcommand",
	Long:  "Git flow feature subcommand",
}

// bugfix
var bugfixCmd = &cobra.Command{
	Use:   "bugfix",
	Short: "Git flow bugfix subcommand",
	Long:  "Git flow bugfix subcommand",
}

// release
var releaseCmd = &cobra.Command{
	Use:   "release",
	Short: "Git flow release subcommand",
	Long:  "Git flow release subcommand",
}

// hotfix
var hotfixCmd = &cobra.Command{
	Use:   "hotfix",
	Short: "Git flow hotfix subcommand",
	Long:  "Git flow hotfix subcommand",
}

// support
var supportCmd = &cobra.Command{
	Use:   "support",
	Short: "Git flow support subcommand",
	Long:  "Git flow support subcommand",
}

// config
var configCmd = &cobra.Command{
	Use:   "config",
	Short: "Git flow config subcommand",
	Long:  "Git flow config subcommand",
}

func init() {
	log.SetFlags(0)
	gitFlow = gfl.NewGitFlow()

	// Hack to get version without initializing the entire app (which requires execution inside a git repo)
	// FIXME: This is a hack, refactor to find a better way to do this
	if len(os.Args) > 1 && os.Args[1] == "version" {
		fmt.Print("Git flow version: ")
		_ = gitFlow.Execute("version")
		fmt.Printf("GitFlowLab version: %s\n", gfl.Version)
		os.Exit(0)
	}

	repo = gfl.NewRepo()
	cfg = gfl.NewConfig(cfgFile, repo)
	gitlabClient = gfl.NewGitlabClient(cfg, repo)
	gitFlowCommand = gfl.NewGitFlowCommand(cfg, gitlabClient, repo, gitFlow)

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.gitflowlab.yaml)")
	rootCmd.Flags().BoolVarP(&verbose, "verbose", "v", false, "Verbose (more) output")

	subcommand := "feature"
	featureCmd.AddCommand(subcommands.ListCommand(subcommand, gitFlowCommand))
	featureCmd.AddCommand(subcommands.StartCommand(subcommand, gitFlowCommand))
	featureCmd.AddCommand(subcommands.FinishCommand(subcommand, gitFlowCommand))
	featureCmd.AddCommand(subcommands.PublishCommand(subcommand, gitFlowCommand))
	featureCmd.AddCommand(subcommands.TrackCommand(subcommand, gitFlowCommand))
	featureCmd.AddCommand(subcommands.DiffCommand(subcommand, gitFlowCommand))
	featureCmd.AddCommand(subcommands.RebaseCommand(subcommand, gitFlowCommand))
	featureCmd.AddCommand(subcommands.CheckoutCommand(subcommand, gitFlowCommand))
	featureCmd.AddCommand(subcommands.PullCommand(subcommand, gitFlowCommand))
	featureCmd.AddCommand(subcommands.RenameCommand(subcommand, gitFlowCommand))
	featureCmd.AddCommand(subcommands.DeleteCommand(subcommand, gitFlowCommand))
	rootCmd.AddCommand(featureCmd)

	subcommand = "bugfix"
	bugfixCmd.AddCommand(subcommands.ListCommand(subcommand, gitFlowCommand))
	bugfixCmd.AddCommand(subcommands.StartCommand(subcommand, gitFlowCommand))
	bugfixCmd.AddCommand(subcommands.FinishCommand(subcommand, gitFlowCommand))
	bugfixCmd.AddCommand(subcommands.PublishCommand(subcommand, gitFlowCommand))
	bugfixCmd.AddCommand(subcommands.TrackCommand(subcommand, gitFlowCommand))
	bugfixCmd.AddCommand(subcommands.DiffCommand(subcommand, gitFlowCommand))
	bugfixCmd.AddCommand(subcommands.RebaseCommand(subcommand, gitFlowCommand))
	bugfixCmd.AddCommand(subcommands.CheckoutCommand(subcommand, gitFlowCommand))
	bugfixCmd.AddCommand(subcommands.PullCommand(subcommand, gitFlowCommand))
	bugfixCmd.AddCommand(subcommands.RenameCommand(subcommand, gitFlowCommand))
	bugfixCmd.AddCommand(subcommands.DeleteCommand(subcommand, gitFlowCommand))
	rootCmd.AddCommand(bugfixCmd)

	subcommand = "release"
	releaseCmd.AddCommand(subcommands.ListCommand(subcommand, gitFlowCommand))
	releaseCmd.AddCommand(subcommands.StartCommand(subcommand, gitFlowCommand))
	releaseCmd.AddCommand(subcommands.FinishCommand(subcommand, gitFlowCommand))
	releaseCmd.AddCommand(subcommands.PublishCommand(subcommand, gitFlowCommand))
	releaseCmd.AddCommand(subcommands.TrackCommand(subcommand, gitFlowCommand))
	releaseCmd.AddCommand(subcommands.DeleteCommand(subcommand, gitFlowCommand))
	rootCmd.AddCommand(releaseCmd)

	subcommand = "hotfix"
	hotfixCmd.AddCommand(subcommands.ListCommand(subcommand, gitFlowCommand))
	hotfixCmd.AddCommand(subcommands.StartCommand(subcommand, gitFlowCommand))
	hotfixCmd.AddCommand(subcommands.FinishCommand(subcommand, gitFlowCommand))
	hotfixCmd.AddCommand(subcommands.PublishCommand(subcommand, gitFlowCommand))
	hotfixCmd.AddCommand(subcommands.DeleteCommand(subcommand, gitFlowCommand))
	rootCmd.AddCommand(hotfixCmd)

	subcommand = "support"
	supportCmd.AddCommand(subcommands.ListCommand(subcommand, gitFlowCommand))
	supportCmd.AddCommand(subcommands.StartCommand(subcommand, gitFlowCommand))
	supportCmd.AddCommand(subcommands.PublishCommand(subcommand, gitFlowCommand))
	supportCmd.AddCommand(subcommands.TrackCommand(subcommand, gitFlowCommand))
	supportCmd.AddCommand(subcommands.DeleteCommand(subcommand, gitFlowCommand))
	rootCmd.AddCommand(supportCmd)

	rootCmd.AddCommand(subcommands.SetupCommand(gitFlowCommand))
	rootCmd.AddCommand(subcommands.VersionCommand(gitFlowCommand))

	configCmd.AddCommand(subcommands.ConfigSetCommand(cfg, repo))
	configCmd.AddCommand(subcommands.ConfigUnsetCommand(cfg, repo))
	rootCmd.AddCommand(configCmd)
}
