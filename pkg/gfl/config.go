package gfl

import (
	"errors"
	"fmt"
	"log"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

const (
	defaultAPIVersion = "4"
	defaultScheme     = "https"
)

type Config interface {
	GetGitlabToken() string
	GetGitlabURL() string
	GetVerbose() bool
}

type cfg struct {
	*viper.Viper

	verbose     bool
	gitlabURL   string
	gitlabToken string
}

func NewConfig(cfgFile string, repo Repo) Config {
	v := viper.New()

	if cfgFile != "" {
		// Use config file from the flag.
		v.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := os.UserHomeDir()
		cobra.CheckErr(err)

		// Search config in home directory with name ".gitflowlab" (without extension).
		v.AddConfigPath(home)
		v.SetConfigType("yaml")
		v.SetConfigName(".gitflowlab")
	}

	v.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	err := v.ReadInConfig()
	if err != nil && !errors.Is(err, viper.ConfigFileNotFoundError{}) {
		log.Fatalf("Error reading config file: %s", err)
	}

	originHost := repo.GetOriginHost()
	originCfg, found := v.GetStringMap("servers")[originHost]
	if !found {
		log.Fatalf("Error: server %s not found in config", originHost)
	}

	originCfgMap := originCfg.(map[string]interface{})

	gitlabToken := ""
	if originCfgMap["token"] != nil {
		gitlabToken = originCfgMap["token"].(string)
	}

	gitlabAPIVersion := defaultAPIVersion
	if originCfgMap["version"] != nil {
		gitlabAPIVersion = originCfgMap["version"].(string)
	}

	scheme := defaultScheme
	if originCfgMap["https"] != nil {
		if !originCfgMap["https"].(bool) {
			scheme = "http"
		}
	}
	gitlabURL := fmt.Sprintf("%s://%s/api/v%s", scheme, originHost, gitlabAPIVersion)

	return &cfg{
		Viper:       v,
		gitlabURL:   gitlabURL,
		gitlabToken: gitlabToken,
	}
}

func (c *cfg) GetGitlabToken() string {
	return c.gitlabToken
}

func (c *cfg) GetGitlabURL() string {
	return c.gitlabURL
}

func (c *cfg) GetVerbose() bool {
	return c.verbose
}
