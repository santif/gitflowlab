package gfl

import (
	"fmt"
	"log"
	"os"
	"regexp"
	"strings"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/config"
	"github.com/go-git/go-git/v5/plumbing"
)

type Repo interface {
	GetOriginURL() string
	GetGitflowMainBranch() string
	GetGitflowDevelopBranch() string
	GetOriginHost() string
	GetCurrentBranch() string
	GetProjectPath() string
	StageAndCommit(file, message string) error
	Push(refName string) error
	GetConfig(subsection, option, def string) string
	GetBoolConfig(subsection, option string, def bool) bool
	SetConfig(subsection, option, value string) error
	DeleteConfig(subsection, option string) error
	GetMainBranchVersion() (string, error)
	GetDevelopBranchVersion() (string, error)
}

type repo struct {
	repoDir string
}

func NewRepo() Repo {
	currentDir, err := os.Getwd()
	if err != nil {
		log.Fatalf("Error getting current directory: %v", err)
	}

	return &repo{
		repoDir: currentDir,
	}
}

func (r *repo) getRepository() *git.Repository {
	result, err := git.PlainOpen(r.repoDir)
	if err != nil {
		log.Fatalf("Error opening repo: %v", err)
	}

	return result
}

func (r *repo) GetOriginURL() string {
	origin, err := r.getRepository().Remote("origin")
	if err != nil {
		log.Fatalf("Error getting origin: %v", err)
	}

	return origin.Config().URLs[0]
}

func (r *repo) GetGitflowMainBranch() string {
	c, err := r.getRepository().Config()
	if err != nil {
		log.Fatalf("Error getting config: %v", err)
	}

	return c.Raw.Section("gitflow").Subsection("branch").Option("master")
}

func (r *repo) GetGitflowDevelopBranch() string {
	c, err := r.getRepository().Config()
	if err != nil {
		log.Fatalf("Error getting config: %v", err)
	}

	return c.Raw.Section("gitflow").Subsection("branch").Option("develop")
}

func (r *repo) GetOriginHost() string {
	originURL := r.GetOriginURL()
	parts := strings.Split(originURL, "@")
	if len(parts) == 1 {
		return ""
	}

	parts = strings.Split(parts[1], ":")
	if len(parts) == 1 {
		return ""
	}

	return parts[0]
}

func (r *repo) GetCurrentBranch() string {
	head, err := r.getRepository().Head()
	if err != nil {
		log.Fatalf("Error getting head: %v", err)
	}

	return head.Name().Short()
}

func (r *repo) GetProjectPath() string {
	originURL := r.GetOriginURL()
	parts := strings.Split(originURL, "@")
	if len(parts) == 1 {
		return ""
	}

	parts = strings.Split(parts[1], ":")
	if len(parts) == 1 {
		return ""
	}

	return strings.Replace(parts[1], ".git", "", 1)
}

func (r *repo) Push(refName string) error {
	opts := &git.PushOptions{
		RemoteName: "origin",
		RefSpecs: []config.RefSpec{
			config.RefSpec(fmt.Sprintf("+refs/heads/%s:refs/heads/%s", refName, refName)),
		},
	}

	err := r.getRepository().Push(opts)
	if err != nil && err != git.NoErrAlreadyUpToDate {
		return err
	}

	return nil
}
func (r *repo) GetConfig(subsection, option, def string) string {
	c, err := r.getRepository().Config()
	if err != nil {
		log.Fatalf("Error getting config: %v", err)
	}

	value := c.Raw.Section("gitflowlab").Subsection(subsection).Option(option)
	if value == "" {
		return def
	}

	return value
}

func (r *repo) GetBoolConfig(subsection, option string, def bool) bool {
	c, err := r.getRepository().Config()
	if err != nil {
		log.Fatalf("Error getting config: %v", err)
	}

	value := c.Raw.Section("gitflowlab").Subsection(subsection).Option(option)
	if value == "" {
		return def
	}

	if value == "true" || value == "1" || value == "yes" {
		return true
	}

	return false
}

func (r *repo) SetConfig(subsection, option, value string) error {
	repository := r.getRepository()

	c, err := repository.Config()
	if err != nil {
		return err
	}

	c.Raw.Section("gitflowlab").Subsection(subsection).SetOption(option, value)
	return repository.SetConfig(c)
}

func (r *repo) DeleteConfig(subsection, option string) error {
	repository := r.getRepository()

	c, err := repository.Config()
	if err != nil {
		return err
	}

	c.Raw.Section("gitflowlab").Subsection(subsection).RemoveOption(option)
	return repository.SetConfig(c)
}

func (r *repo) getVersionFromBranch(branchName string) (string, error) {
	repository := r.getRepository()

	c, err := repository.Config()
	if err != nil {
		return "", err
	}

	vsnRegex := c.Raw.Section("gitflowlab").Subsection("version").Option("regex")
	vsnFilename := c.Raw.Section("gitflowlab").Subsection("version").Option("filename")

	if vsnRegex == "" || vsnFilename == "" {
		return "", ErrConfigVersionFileAndRegexRequired
	}

	re, err := regexp.Compile(vsnRegex)
	if err != nil {
		log.Printf("Warning: invalid regular expression: %s", vsnRegex)
		return "", err
	}

	branch, err := repository.Reference(plumbing.NewBranchReferenceName(branchName), true)
	if err != nil {
		log.Printf("Warning: cannot find reference to branch %s", branchName)
		return "", err
	}

	co, err := repository.CommitObject(branch.Hash())
	if err != nil {
		log.Printf("Warning: cannot find commit object for branch %s", branchName)
		return "", err
	}

	tree, err := repository.TreeObject(co.TreeHash)
	if err != nil {
		log.Printf("Warning: cannot find tree object: %s", branch.Hash().String())
		return "", err
	}

	file, err := tree.File(vsnFilename)
	if err != nil {
		log.Printf("Warning: cannot find object %s", vsnFilename)
		return "", err
	}

	lines, err := file.Lines()
	if err != nil {
		log.Printf("Warning: cannot read file lines")
		return "", err
	}

	for _, line := range lines {
		vsn := re.FindStringSubmatch(line)
		if len(vsn) > 0 {
			return vsn[1], nil
		}
	}

	return "", ErrCurrentVersionNotFound
}

func (r *repo) GetMainBranchVersion() (string, error) {
	c, err := r.getRepository().Config()
	if err != nil {
		return "", err
	}

	branch := c.Raw.Section("gitflow").Subsection("branch").Option("master")
	if branch == "" {
		// Compatibility with older repos by default
		branch = "master"
	}

	return r.getVersionFromBranch(branch)
}

func (r *repo) GetDevelopBranchVersion() (string, error) {
	c, err := r.getRepository().Config()
	if err != nil {
		return "", err
	}

	branch := c.Raw.Section("gitflow").Subsection("branch").Option("develop")
	if branch == "" {
		branch = "develop"
	}

	return r.getVersionFromBranch(branch)
}

func (r *repo) StageAndCommit(file, message string) error {
	w, err := r.getRepository().Worktree()
	if err != nil {
		return err
	}

	_, err = w.Add(file)
	if err != nil {
		return err
	}

	_, err = w.Commit(message, &git.CommitOptions{})
	return err
}
