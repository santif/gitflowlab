package gfl

import (
	"fmt"
	"log"

	"github.com/xanzy/go-gitlab"
)

type MergeRequestData struct {
	SourceBranch string
	TargetBranch string
}

type NewMergeRequestData struct {
	SourceBranch string
	TargetBranch string
	Title        string
	Description  string
}

type GitlabClient interface {
	CreateMergeRequest(mrData NewMergeRequestData) error
	GetMergeRequest(mrData MergeRequestData) (*gitlab.MergeRequest, error)
	IsMergeRequestApproved(mr *gitlab.MergeRequest) bool
	CloseMergeRequestIfNotMerged(mr *gitlab.MergeRequest) error
}

type gitlabClient struct {
	cfg    Config
	repo   Repo
	client *gitlab.Client
}

func NewGitlabClient(cfg Config, repo Repo) GitlabClient {
	client, err := gitlab.NewClient(cfg.GetGitlabToken(), gitlab.WithBaseURL(cfg.GetGitlabURL()))
	if err != nil {
		log.Fatal(err)
	}

	return &gitlabClient{
		cfg:    cfg,
		repo:   repo,
		client: client,
	}
}

func (c *gitlabClient) CreateMergeRequest(mrData NewMergeRequestData) error {
	svc := c.client.MergeRequests
	opts := &gitlab.CreateMergeRequestOptions{
		Title:        gitlab.String(fmt.Sprintf("Draft: %s", mrData.Title)),
		Description:  gitlab.String(mrData.Description),
		SourceBranch: gitlab.String(mrData.SourceBranch),
		TargetBranch: gitlab.String(mrData.TargetBranch),
	}

	mr, resp, err := svc.CreateMergeRequest(c.repo.GetProjectPath(), opts)
	if err != nil {
		return err
	}

	if resp.StatusCode != 201 {
		log.Printf("Unexpected status code: %d", resp.StatusCode)
		return ErrUnexpectedError
	}

	log.Printf("Merge Request created: %v", mr.WebURL)
	return nil
}

func (c *gitlabClient) GetMergeRequest(mrData MergeRequestData) (mr *gitlab.MergeRequest, err error) {
	svc := c.client.MergeRequests
	opts := &gitlab.ListMergeRequestsOptions{
		OrderBy:      gitlab.String("created_at"),
		Sort:         gitlab.String("desc"),
		Scope:        gitlab.String("all"),
		SourceBranch: gitlab.String(mrData.SourceBranch),
		TargetBranch: gitlab.String(mrData.TargetBranch),
	}

	mrs, resp, err := svc.ListMergeRequests(opts)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != 200 {
		log.Printf("Unexpected status code: %d", resp.StatusCode)
		return nil, ErrUnexpectedError
	}

	if len(mrs) > 0 {
		mr, _, err := svc.GetMergeRequest(c.repo.GetProjectPath(), mrs[0].IID, nil)
		if err != nil {
			return nil, err
		}

		return mr, nil
	}

	return mr, ErrMergeRequestNotFound
}

// Checks if MR is approved by the user what MRs is assigned to
func (c *gitlabClient) IsMergeRequestApproved(mr *gitlab.MergeRequest) bool {
	svc := c.client.MergeRequests
	approvals, resp, err := svc.GetMergeRequestApprovals(c.repo.GetProjectPath(), mr.IID)
	if err != nil {
		return false
	}

	if resp.StatusCode != 200 {
		log.Printf("Unexpected status code: %d", resp.StatusCode)
		return false
	}

	users := make(map[string]bool)
	if mr.Assignee != nil {
		users[mr.Assignee.Username] = false
	}

	for _, assignee := range mr.Assignees {
		if assignee != nil {
			users[assignee.Username] = false
		}
	}

	if len(users) == 0 {
		return false
	}

	for _, approval := range approvals.ApprovedBy {
		users[approval.User.Username] = true
	}

	approved := true
	for _, user := range users {
		if !user {
			approved = false
		}
	}

	return approved
}

func (c *gitlabClient) CloseMergeRequestIfNotMerged(mr *gitlab.MergeRequest) error {
	svc := c.client.MergeRequests
	opts := &gitlab.UpdateMergeRequestOptions{
		StateEvent: gitlab.String("close"),
	}

	_, resp, err := svc.UpdateMergeRequest(c.repo.GetProjectPath(), mr.IID, opts)
	if err != nil {
		return err
	}

	if resp.StatusCode != 200 {
		log.Printf("Unexpected status code: %d", resp.StatusCode)
		return ErrUnexpectedError
	}

	return nil
}
