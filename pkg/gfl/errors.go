package gfl

import (
	"errors"
)

var (
	ErrOperationNotSupported                        = errors.New("operation not supported")
	ErrMergeRequestNotFound                         = errors.New("merge request not found")
	ErrMergeRequestIsRequired                       = errors.New("merge request is required")
	ErrMergeRequestIsNotApproved                    = errors.New("merge request is not approved")
	ErrMergeRequestCannotBeMerged                   = errors.New("merge request cannot be merged")
	ErrMergeRequestHasConflicts                     = errors.New("merge request has conflicts")
	ErrMergeRequestHasBlockingDiscussionsUnresolved = errors.New("merge request has blocking discussions unresolved")
	ErrMergeRequestIsWorkInProgress                 = errors.New("merge request is in WIP (work in progress) or in Draft status")
	ErrVersionIsRequired                            = errors.New("version is required")
	ErrCurrentVersionNotFound                       = errors.New("current version not found")
	ErrConfigVersionFileAndRegexRequired            = errors.New("version file and regex configs are required")
	ErrCommandIsRequired                            = errors.New("command is required")
	ErrUnexpectedError                              = errors.New("unexpected error")
)
