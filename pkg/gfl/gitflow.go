package gfl

import (
	"os"
	"os/exec"
	"strings"
)

type GitFlow interface {
	Execute(args ...string) error
	GetVersion() (string, error)
}

type gitFlow struct {
}

func NewGitFlow() GitFlow {
	return &gitFlow{}
}

func (gf *gitFlow) Execute(args ...string) error {
	args = append([]string{"flow"}, args...)
	cmd := exec.Command("git", args...)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}

func (gf *gitFlow) GetVersion() (string, error) {
	cmd := exec.Command("git", "flow", "version")
	bs, err := cmd.Output()
	if err != nil {
		return "", err
	}

	vsnStr := strings.Split(string(bs), " (AVH Edition)")[0]
	return vsnStr, nil
}
