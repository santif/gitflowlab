package gfl

import (
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"strings"

	"github.com/Masterminds/semver/v3"
)

type SetupOptions struct {
	VersionRegexString           string
	VersionFilename              string
	MergeRequestApprovalRequired bool
}

type GitFlowCommand interface {
	Version() error
	SetCommand(cmd string) GitFlowCommand
	Setup(args []string, opts SetupOptions) error
	Start(args []string) error
	Publish(args []string) error
	Finish(args []string) error
	List(args []string) error
	Track(args []string) error
	Diff(args []string) error
	Rebase(args []string) error
	Checkout(args []string) error
	Pull(args []string) error
	Rename(args []string) error
	Delete(args []string) error
}

type gitFlowCommand struct {
	cmd    string
	cfg    Config
	client GitlabClient
	repo   Repo
	gf     GitFlow
}

func NewGitFlowCommand(cfg Config, client GitlabClient, repo Repo, gf GitFlow) GitFlowCommand {
	return &gitFlowCommand{"", cfg, client, repo, gf}
}

//
// Subcommands
//

func (c *gitFlowCommand) SetCommand(cmd string) GitFlowCommand {
	c.cmd = cmd
	return c
}

func (c *gitFlowCommand) Version() error {
	fmt.Print("Git flow version: ")
	err := c.gf.Execute("version")
	fmt.Printf("GitFlowLab version: %s\n", Version)
	return err
}

func (c *gitFlowCommand) Setup(args []string, opts SetupOptions) error {
	args = append([]string{"init", "-d", "-f", "-t", "v"}, args...)
	err := c.gf.Execute(args...)
	if err != nil {
		return err
	}

	if opts.VersionFilename != "" {
		c.repo.SetConfig("version", "filename", opts.VersionFilename)
	} else {
		c.repo.DeleteConfig("version", "filename")
	}

	if opts.VersionRegexString != "" {
		c.repo.SetConfig("version", "regex", opts.VersionRegexString)
	} else {
		c.repo.DeleteConfig("version", "regex")
	}

	if !opts.MergeRequestApprovalRequired {
		c.repo.SetConfig("mergerequest", "approvalrequired", "false")
	} else {
		c.repo.DeleteConfig("mergerequest", "approvalrequired")
	}

	err = c.repo.Push(c.repo.GetGitflowMainBranch())
	if err != nil {
		return err
	}

	err = c.repo.Push(c.repo.GetGitflowDevelopBranch())
	if err != nil {
		return err
	}

	return nil
}

func (c *gitFlowCommand) Start(args []string) error {
	if c.cmd == "" {
		return ErrCommandIsRequired
	}

	if len(args) == 0 && c.cmd == "release" {
		args = []string{"minor"}
	}

	if len(args) == 0 && c.cmd == "hotfix" {
		args = []string{"patch"}
	}

	var err error
	var newVersion string
	if c.cmd == "release" || c.cmd == "hotfix" {
		args, err = getAutoVersion(c.cmd, c.repo, args)
		if err != nil {
			return err
		}

		newVersion = args[0]
	}

	args = append([]string{c.cmd, "start"}, args...)
	err = c.gf.Execute(args...)
	if err != nil {
		return err
	}

	if c.cmd != "release" && c.cmd != "hotfix" {
		return nil
	}

	// Update version
	versionFile := c.repo.GetConfig("version", "filename", "")
	versionRegex := c.repo.GetConfig("version", "regex", "")
	if versionFile == "" || versionRegex == "" || newVersion == "" {
		return nil
	}

	err = updateVersionInPlace(
		c.cmd,
		c.repo,
		c.repo.GetConfig("version", "filename", ""),
		c.repo.GetConfig("version", "regex", ""),
		newVersion,
	)
	if err != nil {
		log.Printf("Failed to update version in %s: %s", versionFile, err)
		return nil
	}

	err = c.repo.StageAndCommit(versionFile, fmt.Sprintf("Bump version to %s", newVersion))
	if err != nil {
		log.Printf("Warning: cannot commit version update: %s", err)
		return nil
	}

	return nil
}

func (c *gitFlowCommand) Publish(args []string) error {
	if c.cmd == "" {
		return ErrCommandIsRequired
	}

	args = append([]string{c.cmd, "publish"}, args...)
	err := c.gf.Execute(args...)
	if err != nil {
		return err
	}

	sourceBranch := c.determineSourceBranch(args)
	targetBranch := c.determineTargetBranch(args)

	title := sourceBranch
	description := "TODO: Complete description."

	mergeRequestData := NewMergeRequestData{
		SourceBranch: sourceBranch,
		TargetBranch: targetBranch,
		Title:        title,
		Description:  description,
	}
	err = c.client.CreateMergeRequest(mergeRequestData)

	// FIXME if merge exists, print its URL (isn't an error)

	return err
}

func (c *gitFlowCommand) Finish(args []string) error {
	if c.cmd == "" {
		return ErrCommandIsRequired
	}

	if c.cmd == "support" {
		return ErrOperationNotSupported
	}

	sourceBranch := c.determineSourceBranch(args)
	targetBranch := c.determineTargetBranch(args)

	mergeRequestData := MergeRequestData{
		SourceBranch: sourceBranch,
		TargetBranch: targetBranch,
	}
	mr, err := c.client.GetMergeRequest(mergeRequestData)
	if err != nil && err != ErrMergeRequestNotFound {
		return err
	}

	if err == ErrMergeRequestNotFound {
		if c.cmd == "release" || c.cmd == "hotfix" {
			log.Printf("Merge request not found. Please run `gfl %s publish`.", c.cmd)
			return ErrMergeRequestIsRequired
		}
	}

	log.Printf("Merge Request URL: %s", mr.WebURL)

	if mr.MergeStatus == "merged" {
		args = append([]string{c.cmd, "finish", "-F", "-p", "--pushtag", "-m", mr.Title}, args...)
		err = c.gf.Execute(args...)
		if err != nil {
			return err
		}

		// TODO other tasks?

		return nil
	}

	if mr.MergeStatus != "can_be_merged" {
		log.Printf("Merge request cannot be merged: %s", mr.MergeStatus)
		return ErrMergeRequestCannotBeMerged
	}

	if mr.WorkInProgress {
		return ErrMergeRequestIsWorkInProgress
	}

	if mr.HasConflicts {
		return ErrMergeRequestHasConflicts
	}

	if !mr.BlockingDiscussionsResolved {
		return ErrMergeRequestHasBlockingDiscussionsUnresolved
	}

	if c.repo.GetBoolConfig("mergerequest", "approvalrequired", true) {
		if !c.client.IsMergeRequestApproved(mr) {
			return ErrMergeRequestIsNotApproved
		}
	}

	pushMainBranch := false
	switch c.cmd {
	case "release":
		baseArgs := []string{c.cmd, "finish", "-m", mr.Title, "-F", "--ff-master", "-p", "--pushtag"}

		if isNoEditEnabled(c.gf) {
			args = append(append(baseArgs, "--noedit"), args...)
		} else {
			args = append(baseArgs, args...)
		}

		pushMainBranch = true
	case "hotfix":
		args = append([]string{c.cmd, "finish", "-F", "-p", "-m", mr.Title}, args...)
		pushMainBranch = true
	case "bugfix", "feature":
		args = append([]string{c.cmd, "finish", "-F", "--push", "-S"}, args...)
	default:
		args = append([]string{c.cmd, "finish", "-F", "--push"}, args...)
	}

	err = c.gf.Execute(args...)
	if err != nil {
		return err
	}

	if pushMainBranch {
		err = c.repo.Push(c.repo.GetGitflowMainBranch())
		if err != nil {
			log.Printf("Warning: cannot push main branch: %s", err)
		}
	}

	err = c.repo.Push(c.repo.GetGitflowDevelopBranch())
	if err != nil {
		log.Printf("Warning: cannot push develop branch: %s", err)
	}

	return nil
}

func (c *gitFlowCommand) List(args []string) error {
	if c.cmd == "" {
		return ErrCommandIsRequired
	}

	args = append([]string{c.cmd, "list"}, args...)
	return c.gf.Execute(args...)
}

func (c *gitFlowCommand) Track(args []string) error {
	if c.cmd == "" {
		return ErrCommandIsRequired
	}

	args = append([]string{c.cmd, "track"}, args...)
	return c.gf.Execute(args...)
}

func (c *gitFlowCommand) Diff(args []string) error {
	if c.cmd == "" {
		return ErrCommandIsRequired
	}

	args = append([]string{c.cmd, "diff"}, args...)
	return c.gf.Execute(args...)
}

func (c *gitFlowCommand) Rebase(args []string) error {
	if c.cmd == "" {
		return ErrCommandIsRequired
	}

	args = append([]string{c.cmd, "rebase"}, args...)
	return c.gf.Execute(args...)
}

func (c *gitFlowCommand) Checkout(args []string) error {
	if c.cmd == "" {
		return ErrCommandIsRequired
	}

	args = append([]string{c.cmd, "checkout"}, args...)
	return c.gf.Execute(args...)
}

func (c *gitFlowCommand) Pull(args []string) error {
	if c.cmd == "" {
		return ErrCommandIsRequired
	}

	args = append([]string{c.cmd, "pull"}, args...)
	return c.gf.Execute(args...)
}

func (c *gitFlowCommand) Rename(args []string) error {
	if c.cmd == "" {
		return ErrCommandIsRequired
	}

	args = append([]string{c.cmd, "rename"}, args...)
	return c.gf.Execute(args...)
}

func (c *gitFlowCommand) Delete(args []string) error {
	if c.cmd == "" {
		return ErrCommandIsRequired
	}

	args = append([]string{c.cmd, "delete"}, args...)
	return c.gf.Execute(args...)
}

func (c *gitFlowCommand) determineSourceBranch(args []string) string {
	// TODO Take into account args (branch name)
	return c.repo.GetCurrentBranch()
}

func (c *gitFlowCommand) determineTargetBranch(args []string) string {
	if c.cmd == "release" || c.cmd == "hotfix" {
		return c.repo.GetGitflowMainBranch()
	}

	return c.repo.GetGitflowDevelopBranch()
}

//
// Private functions
//

func isNoEditEnabled(gitFlow GitFlow) bool {
	vsn, err := gitFlow.GetVersion()
	if err != nil {
		return false
	}

	v, err := semver.NewVersion(vsn)
	if err != nil {
		return false
	}

	// `--noedit` flag is available since GitFlow 1.12.4-dev0
	return v.GreaterThan(semver.MustParse("1.12.3"))
}

func getAutoVersion(cmd string, repo Repo, args []string) ([]string, error) {
	if len(args) == 0 {
		return nil, ErrVersionIsRequired
	}

	version := args[0]
	isVersionKeyword := version == "minor" || version == "major" || version == "patch"

	if isVersionKeyword {
		currentVersion, err := getCurrentVersion(cmd, repo)
		if err != nil {
			log.Printf("Warning: cannot get current version: %s", err)
			return nil, err
		}

		newVsn, err := incrementCurrentVersion(repo, currentVersion, version)
		if err != nil {
			log.Printf("Warning: cannot increment version: %s", err)
			return nil, err
		}

		argsTail := args[1:]
		args = append([]string{newVsn}, argsTail...)
	}

	return args, nil
}

func incrementCurrentVersion(repo Repo, currentVersion, keyword string) (string, error) {
	curVsn := semver.MustParse(currentVersion)
	var newVsn semver.Version

	switch keyword {
	case "major":
		newVsn = curVsn.IncMajor()
	case "minor":
		newVsn = curVsn.IncMinor()
	case "patch":
		newVsn = curVsn.IncPatch()
	}

	return newVsn.String(), nil
}

func getCurrentVersion(cmd string, repo Repo) (string, error) {
	if cmd == "release" || cmd == "hotfix" {
		return repo.GetMainBranchVersion()
	}

	return repo.GetDevelopBranchVersion()
}

func updateVersionInPlace(cmd string, repo Repo, fileName string, versionRegex string, text string) error {
	re := regexp.MustCompile(versionRegex)

	input, err := ioutil.ReadFile(fileName)
	if err != nil {
		log.Fatalln(err)
	}

	lines := strings.Split(string(input), "\n")

	for i, line := range lines {
		parts := re.FindStringSubmatch(line)

		if len(parts) > 1 {
			lines[i] = strings.Replace(line, parts[1], text, 1)
			break
		}
	}

	output := strings.Join(lines, "\n")
	err = ioutil.WriteFile(fileName, []byte(output), 0644)
	if err != nil {
		return err
	}

	return nil
}
