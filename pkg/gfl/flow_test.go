package gfl

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIncrementCurrentVersion_Minor(t *testing.T) {
	repo := new(MockRepo)

	result, err := incrementCurrentVersion(repo, "0.1.0", "minor")

	assert.NoError(t, err)
	assert.Equal(t, "0.2.0", result)

	repo.AssertExpectations(t)
}
